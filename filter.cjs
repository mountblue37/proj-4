function filter(elements = [], cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    let filteredArr = []
    for (let elementIndex = 0; elementIndex < elements.length; elementIndex++) {
        if (cb(elements[elementIndex], elementIndex, elements) === true) {
            filteredArr.push(elements[elementIndex])
        }
    }
    return filteredArr
}

module.exports = filter