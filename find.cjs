function find(elements = [], cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
    for (let elementIndex = 0; elementIndex < elements.length; elementIndex++) {
        if (cb(elements[elementIndex]) === true) {
            return elements[elementIndex]
        }
    }
    return undefined
}

module.exports = find

