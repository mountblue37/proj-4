function flatten(elements=[], depth = 1) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    let result = []
    let index = 0
    while(index < elements.length) {
        if(elements[index] === undefined) {
            index++
            continue
        }
        if(Array.isArray(elements[index]) && depth > 0) {
            let curr = depth
            result = result.concat(flatten(elements[index++], --curr))
        } else {
            result.push(elements[index++])
        } 
    }
    return result
}

module.exports = flatten