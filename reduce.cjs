function reduce(elements = [], cb, startingValue) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
    let result
    let elementIndex
    if (startingValue !== undefined) {
        elementIndex = 0
        result = startingValue
    } else {
        elementIndex = 1
        result = elements[0]
    }

    for (elementIndex; elementIndex < elements.length; elementIndex++) {
        result = cb(result, elements[elementIndex], elementIndex, elements)
    }
    return result
}

module.exports = reduce
